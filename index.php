<?php
require_once("logica-usuario.php");
require_once("cabecalho.php");

?>
<h1>Bem vindo!</h1>
<?php
if(usuarioEstaLogado()){
?>
    <p class="txt alert-success">Você está logado como <?= usuarioLogado()?></p>
    <a href="logout.php" class="btn btn-primary">Logout</a>     

<?php
}else{
?>

    <div class="container">
            <div class="panel panel-primary">
                <h2>Login</h2>
                <form action="login.php" method="post">
                    <div class="form-group">
                        <label>Email
                            <input name="email" type="email">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Senha
                            <input name="senha" type="password">
                        </label>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Logar</button>
                    </div>
                </form>   
            </div> 
     </div>  

<?php
    }
include("rodape.php"); ?>

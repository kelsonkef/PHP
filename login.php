<?php
require_once("dao-usuario.php");
require_once("logica-usuario.php");

$email = $_POST["email"];
$senha = $_POST["senha"];

$usuario = pegaUsuario($conexao, $email, $senha);

if($usuario == null){
    $_SESSION["danger"] = "Usuario ou Senha Invalido.";
    header("Location: index.php");
}else{
    logarUsuario($email);
    $_SESSION["success"] = "Usuario Logado Com Sucesso!";
    header("Location: index.php");
}
die();

?>
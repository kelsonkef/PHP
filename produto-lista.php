<?php 
require_once("dao-produto.php");
require_once("logica-usuario.php");
require_once("cabecalho.php");

$produtos = listarProduto($conexao);
?>
   
   
<table class="table table-striped table-bordered">
        <th>Nome</th>
        <th>Preço</th>
        <th>Descrição</th>
        <th>Categoria</th>

<?php if(usuarioEstaLogado()): ?>
        <th>Alterar</th>
        <th>Remover</th>
    <?php
    endif;
    foreach($produtos as $produto):
    ?>
        <tr>
            <td><?=$produto["nome"]?></td>
            <td><?=$produto["preco"]?></td>
            <td><?=substr($produto["descricao"], 0, 40)?></td>
            <td><?=$produto["categoria_nome"]?></td>
            
    <?php if(usuarioEstaLogado()): ?>
                <td>
                    <form action="produto-alterar-formulario.php" method="post">
                        <input type="hidden" name="id" value="<?=$produto['id']?>">
                        <button class="btn btn-primary" type="submit">Alterar</button>
                    </form>
                </td>
                <td>
                    <form action="produto-deleta.php" method="post">
                        <input type="hidden" name="id" value="<?=$produto['id']?>">
                        <button class="btn btn-danger" type="submit">Remover</button>
                    </form>
                </td>
            <?php endif?>
        </tr>

    <?php
    endforeach
    ?>
        
</table>


<?php
include("rodape.php");
?>